package plugins.danyfel80.library.gson;
import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginLibrary;

/**
 * Icy wrapper for the Gson library.
 * @author Daniel Felipe Gonzalez Obando
 */
public class GsonPlugin extends Plugin implements PluginLibrary
{

}
